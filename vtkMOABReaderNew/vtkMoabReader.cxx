#include "vtkMoabReader.h"

#include "SimpleMoab.h"
#include "DataSetConverter.h"
#include "moab/Core.hpp"

#include "vtkSortDataArray.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkUnstructuredGrid.h"
//#include "../SelectionAlgorithm/MOABSelectionAlgorithm.h"

vtkStandardNewMacro(vtkMoabReader)
//------------------------------------------------------------------------------
vtkMoabReader::vtkMoabReader()
  {
  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(1);
  this->FileName = NULL;
  }

//------------------------------------------------------------------------------
vtkMoabReader::~vtkMoabReader()
  {
  }

//------------------------------------------------------------------------------
int vtkMoabReader::FillOutputPortInformation(int port, vtkInformation* info)
{
  if(port == 0)
    {
      info->Set(vtkDataObject::DATA_TYPE_NAME(),"vtkMultiBlockDataSet");
    }
  /*else if(port == 1)
    {
      info->Set(vtkDataObject::DATA_TYPE_NAME(),"vtkMoabDatabase");
    }
  */
  return 1;
}

//------------------------------------------------------------------------------
int vtkMoabReader::RequestInformation(vtkInformation *request,
                       vtkInformationVector **inputVector,
                       vtkInformationVector *outputVector)
{

  //todo. Walk the file and display all the 2d and 3d elements that the users
  //could possibly want to load
  return this->Superclass::RequestInformation(request,inputVector,outputVector);
}

//------------------------------------------------------------------------------
int vtkMoabReader::RequestData(vtkInformation *vtkNotUsed(request),
                vtkInformationVector **vtkNotUsed(inputVector),
                vtkInformationVector *outputVector)
{
  //First pass is lets load in all 3d elements in a block called Volumes,
  //and load all 2d elements in a block called Surfaces

  vtkInformation* outInfo;// = outputVector->GetInformationObject(1);
  //vtkMoabDatabase *output2 = vtkMoabDatabase::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  outInfo = outputVector->GetInformationObject(0);
  vtkMultiBlockDataSet *output =
  vtkMultiBlockDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));


  vtkNew<vtkMultiBlockDataSet> volumeRoot;
  vtkNew<vtkMultiBlockDataSet> surfaceRoot;
  vtkNew<vtkMultiBlockDataSet> matGroupRoot;
  vtkNew<vtkMultiBlockDataSet> talVolGroupRoot;
  vtkNew<vtkMultiBlockDataSet> talSurfGroupRoot;

  const int blockIndex = output->GetNumberOfBlocks();
  output->SetBlock(blockIndex,volumeRoot.GetPointer());
  output->SetBlock(blockIndex+1,surfaceRoot.GetPointer());
  output->SetBlock(blockIndex+2,matGroupRoot.GetPointer());
  output->SetBlock(blockIndex+3,talVolGroupRoot.GetPointer());
  output->SetBlock(blockIndex+4,talSurfGroupRoot.GetPointer());

  //boring work, set the names of the blocks
  output->GetMetaData(blockIndex)->Set(vtkCompositeDataSet::NAME(), "Volumes");
  output->GetMetaData(blockIndex+1)->Set(vtkCompositeDataSet::NAME(), "Surfaces");
  output->GetMetaData(blockIndex+2)->Set(vtkCompositeDataSet::NAME(), "Materials");
  output->GetMetaData(blockIndex+3)->Set(vtkCompositeDataSet::NAME(), "Volume Tallies");
  output->GetMetaData(blockIndex+4)->Set(vtkCompositeDataSet::NAME(), "Surface Tallies");
    
  smoab::GeomTag geom3Tag(3);
  smoab::GeomTag geom2Tag(2);
  smoab::GeomTag geom1Tag(2);
  smoab::NeumannTag neTag;
  smoab::DirichletTag diTag;
  smoab::MaterialTag matTag;
  smoab::GroupTag grouTag;

  std::string talStr("tal");
  std::string matStr("mat");

  smoab::Interface interface(this->FileName);

  //output2->Moab = new moab::Core();
  std::string str = std::string(this->FileName);  

  this->CreateGeomBlocks(volumeRoot, &interface, &geom3Tag);
  this->CreateGeomBlocks(surfaceRoot, &interface, &geom2Tag);
  this->CreateTagBlocks(matGroupRoot, &interface, &geom3Tag, matStr);
  this->CreateTagBlocks(talVolGroupRoot, &interface, &geom3Tag, talStr);
  this->CreateTagBlocks(talSurfGroupRoot, &interface, &geom2Tag, talStr);

  /*
  outInfo = outputVector->GetInformationObject(1);
  vtkMoabDatabase *output2 = vtkMoabDatabase::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
  output2->Moab;// = new moab::Core();
  output2->Moab->load_file(str.c_str());
  */


  //this->GetSelFilterList();
  //this->SetSelFilter(vecStr.front());
  return 1;
}

//------------------------------------
enum vtkMoabReader::caseType
    {
    DECOMPOSED_CASE_APPENDED = 0,
    RECONSTRUCTED_CASE = 1,
    DECOMPOSED_CASE_MULTIBLOCK = 2
    };


//------------------------------------------------------------------------------
void vtkMoabReader::CreateSubBlocks(vtkNew<vtkMultiBlockDataSet> & root,
                                    smoab::Interface* interface,
                                    smoab::Tag const* parentTag,
                                    smoab::Tag const* extractTag)
{
  // With the change to handle greater detail within ParaView, this is no longer used, but still kept her for the time being.
  if(!extractTag)
    {
    extractTag = parentTag;
    }
  //basic premise: query the database for all tagged elements and create a new
  //multiblock elemenent for each

  smoab::DataSetConverter converter(*interface,extractTag);
  converter.readMaterialIds(true);
  converter.readProperties(true);

  smoab::EntityHandle rootHandle = interface->getRoot();
  smoab::Range parents = interface->findEntityRootParents(rootHandle);
  smoab::Range dimEnts = interface->findEntitiesWithTag(*parentTag,
                                                       rootHandle);

  smoab::Range geomParents = smoab::intersect(parents,dimEnts);

  parents.clear(); //remove this range as it is unneeded
  dimEnts.clear();

  //now each item in range can be extracted into a different grid
  typedef smoab::Range::iterator iterator;
  vtkIdType index = 0;
  for(iterator i=geomParents.begin(); i != geomParents.end(); ++i)
    {
    vtkNew<vtkUnstructuredGrid> block;
    //fill the dataset with geometry and properties
    converter.fill(*i, block.GetPointer(),index);

    //only add it if we have cells found
    int numCells = block->GetNumberOfCells();
    if(numCells > 0)
      {
      root->SetBlock(index,block.GetPointer());
      std::string name = interface->name(*i);
      if(name.size() > 0)
        {
        root->GetMetaData(index)->Set(vtkCompositeDataSet::NAME(), name.c_str());
        }
      ++index;
      }
    }
}

//------------------------------------------------------------------------------
void vtkMoabReader::CreateGeomBlocks(vtkNew<vtkMultiBlockDataSet> & root,
                                    smoab::Interface* interface,
                                     smoab::Tag const* t)
{
  smoab::Range entSets;
  // We find all of the large entity sets (volumes, surfaces), that match our dimensionality.
  smoab::Range tri = interface->findEntitiesWithTag(*t,0,moab::MBENTITYSET);

  smoab::DataSetConverter converter(*interface,t);
  converter.readMaterialIds(true);
  converter.readProperties(true);

  //now each item in range can be extracted into a different grid
  typedef smoab::Range::iterator iterator;
  vtkIdType index = 0;
  for(iterator i=tri.begin(); i != tri.end(); ++i)
    {
    vtkNew<vtkUnstructuredGrid> block;
    //fill the dataset with geometry and properties
    converter.fill(*i, block.GetPointer(),index);

    //only add it if we have cells found
    int numCells = block->GetNumberOfCells();
    if(numCells > 0)
      {
      // This Cell Data Structure will hold the Moab information on the vtkOjbect
      vtkCellData * CD;
      vtkIdType j;


      root->SetBlock(index,block.GetPointer());
      
      std::string name = interface->name(*i);
      if(name.size() > 0)
        {
        root->GetMetaData(index)->Set(vtkCompositeDataSet::NAME(), name.c_str());
        }
      ++index;
      }
    }

}

//------------------------------------------------------------------------------
void vtkMoabReader::CreateTagBlocks(vtkNew<vtkMultiBlockDataSet> & root,
                                    smoab::Interface* interface,
                                    smoab::Tag const* t, 
                                    std::string tagStr)
{
  smoab::Range entSets,ents;
  entSets = interface->findEntities(0, moab::MBENTITYSET);
  typedef smoab::Range::iterator iterator;
  smoab::DataSetConverter converter(*interface,t);
  converter.readMaterialIds(true);
  converter.readProperties(true);

  //now each item in range can be extracted into a different grid
  vtkIdType index = 0;
  for(iterator i=entSets.begin(); i != entSets.end(); ++i)
    {
      std::string str = interface->name(*i);
      if ( (str.size() == 0) || (str.substr(0, 3) != tagStr) )
        {
           continue;
        }
    vtkNew<vtkMultiBlockDataSet> block;
    //fill the dataset with geometry and properties
    smoab::Range groupSets = interface->findEntitiesWithTag(*t,*i,moab::MBENTITYSET);
    vtkIdType index2 = 0;

    if(groupSets.begin()==groupSets.end()){continue;}

    for(iterator ii=groupSets.begin(); ii != groupSets.end(); ++ii)
      {
        vtkNew<vtkUnstructuredGrid> block2;
    converter.fill(*ii, block2.GetPointer(),index2);

    //only add it if we have cells found
    int numCells = block2->GetNumberOfCells();
    if(numCells > 0)
      {
      // This Cell Data Structure will hold the Moab information on the vtkOjbect
      vtkCellData * CD;
      vtkIdType j;

      block->SetBlock(index2,block2.GetPointer());
      std::string str2 = interface->name(*ii);
      block->GetMetaData(index2)->Set(vtkCompositeDataSet::NAME(), str2.c_str());
      
      ++index2;
      }
    
    }
    root->SetBlock(index,block.GetPointer());
    root->GetMetaData(index)->Set(vtkCompositeDataSet::NAME(), str.c_str());
    ++index;
    }
}

//------------------------------------------------------------------------------
void vtkMoabReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
 
