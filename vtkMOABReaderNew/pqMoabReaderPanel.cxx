#include "pqMoabReaderPanel.h"

#include "vtkPVConfig.h" // for PARAVIEW_VERSIONs
#include <sstream>
#include <iostream>

// UI
#include "pqAnimationScene.h"
#include "pqApplicationCore.h"
#include "pqCollapsedGroup.h"
#include "pqNamedWidgets.h"
#include "pqObjectBuilder.h"
#include "pqPipelineRepresentation.h"
#include "pqPropertyLinks.h"
#include "pqServerManagerModel.h"
#include "pqView.h"
#include "pqSelectionManager.h"
#include "pqActiveObjects.h"
#include "pqActiveView.h"

// server manager
#include "vtkSMDoubleVectorProperty.h"
#include "vtkSMEnumerationDomain.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMRenderViewProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMStringVectorProperty.h"
#include "moab/Core.hpp"
#include "moab/Interface.hpp"
#include "vtkSmartPointer.h"
#include "vtkExtractSelection.h"
#include "vtkSelection.h"
#include "vtkPVDataSetAttributesInformation.h"
#include "vtkPVDataInformation.h"
#include "vtkPVArrayInformation.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkDataArray.h"
#include "vtkFieldData.h"
#include "vtkExtractSelectedIds.h"
#include "vtkPVInformation.h"
#include "vtkPVSelectionInformation.h"
#include "vtkPVView.h"
#include "vtkPVRenderView.h"
#include "vtkSMPropertyIterator.h"

// Qt
#include <QList>
#include <QAbstractButton>
#include <QDebug>
#include <QByteArray>
#include <QMessageBox>

// VTK
#include "vtkClientServerStream.h"
#include "vtkEventQtSlotConnect.h"
#include "vtkUnstructuredGrid.h"
#include "vtkPointData.h"
#include "vtkDataArray.h"
#include "vtkCellData.h"
#include "vtkCellArray.h"
#include "vtkIdTypeArray.h"

pqMoabReaderPanel::pqMoabReaderPanel(pqProxy* pxy, QWidget* p)
: pqLoadedFormObjectPanel(":/MoabReaderPanel/pqMoabReaderPanel.ui", pxy, p)
{
  // Set-up
  this->SelectionSource = 0;
  this->SelectionInputSource = 0;

  this->core = pqApplicationCore::instance();

  // MOAB-style error-check throughout by this check
  moab::ErrorCode rval;

  // Get the FileName the user opened with the vtkMoabReader and open the MOAB interface.
  this->sourceProxy = pxy->getProxy();
  vtkSMStringVectorProperty *smSP = vtkSMStringVectorProperty::SafeDownCast(this->sourceProxy->GetProperty("FileName"));
  this->fn = smSP->GetElement(0);

  moab::EntityHandle input_set;
  rval = this->MBI()->create_meshset( MESHSET_SET, input_set );
  rval = this->MBI()->load_file( this->fn, &input_set );

  mbSel = new MOABSelectionAlgorithm();

  this->selEnts = moab::Range();
  setupGUI();
  this->InputPort = 0;
}

pqMoabReaderPanel::~pqMoabReaderPanel()
{
}

pqDataRepresentation* pqMoabReaderPanel::getSelectionRepresentation() const
{

  if(this->InputPort && this->ActiveView)
    {
    }
  return NULL;

}

void pqMoabReaderPanel::extractEntityHandleSelection()
{

  /*
  if(!(this->SelOutputPort))
    {
      return;
    }

  vtkPVDataInformation* dataInformation = NULL;
  if(this->SelOutputPort->getOutputPortProxy())
    {
      dataInformation = this->SelOutputPort->getDataInformation();
    }

  // if there isn't information, there's nothing we can do, so leave.
  if( !dataInformation)
    {
      return;
    }
  //*/

  // ----------------/2013-01-03/--------------------------------
  // Grab the current pipeline instance and get the selection manager
  pqSelectionManager* selMan = (pqSelectionManager*)(
                                                     pqApplicationCore::instance()->manager("SelectionManager"));
  // If it's not there, we don't have selection manager, so exit.
  // We don't clear the current entity set list, because there could be some selected on the panel.
  if(!selMan)
    {
      return;
    }


  this->InputPort = selMan->getSelectedPort();
  if(!this->InputPort)
    {
      return;
    }

  /*
  vtkSmartPointer<vtkExtractSelection> extrSel = vtkSmartPointer<vtkExtractSelection>::New();

  //extrSel->SetInputConnection(0,);
  vtkSmartPointer<vtkSMSourceProxy> sourcePrxy = this->InputPort->getSelectionInput();
  vtkSMOutputPort *outP = sourcePrxy->GetOutputPort((uint)0);
  extrSel->SetInputConnection(1,outP);
  extrSel->Update()
  ///////////////////////////////////////////////////
  vtkPVDataInformation* dataInformation = NULL;
  dataInformation = this->InputPort->getDataInformation();

  // if there isn't information, there's nothing we can do, so leave.
  if( !dataInformation)
    {
      return;
    }

  vtkPVDataSetAttributesInformation* info[3];
  info[0] = dataInformation->GetPointDataInformation();
  info[1] = dataInformation->GetCellDataInformation();
  info[2] = dataInformation->GetFieldDataInformation();

  for(int k=0; k<3; k++)
    {
      int numArrays = info[k]->GetNumberOfArrays();
      // Here we'll look through all the arrays until we find the Entity ID array.
      for(int i=0; i<numArrays; i++)
        {
          int tmp = i;
          vtkPVArrayInformation* arrayInfo = info[k]->GetArrayInformation(tmp);
          QString arrayName = QString(arrayInfo->GetName());
          if( strcmp(arrayName.toStdString().c_str(),"EntityId") == 0 )
            {
              QTreeWidgetItem *item = new QTreeWidgetItem;
              item->setText(0,arrayName);
              this->selTree->addTopLevelItem(item);
              int numComponents = arrayInfo->GetNumberOfComponents();
              int numTuples = arrayInfo->GetNumberOfTuples();
              std::stringstream stt, std;
              stt << numTuples;
              std << numComponents;
              double* dbl = arrayInfo->GetComponentRange(0);
              item->setText(0,tr("Comps:%1 Range1:%2 Range2:%3").arg(numComponents).arg());
              for(int j=0; j<numComponents; j++)
                {
                  // Do something here to get the entity handles in a range.
                }
            }
        }
    }
  //*/
  ///////////////////////////////////////////////////
 
  // ----------------/2013-01-07/--------------------------------
  pqActiveView* pqAV = &pqActiveView::instance();
  pqView* view = pqAV->current();

  pqRenderView* renView = qobject_cast<pqRenderView*>(view);

  pqDataRepresentation* repr = this->InputPort->getRepresentation(view);
  QTreeWidgetItem* itemError = new QTreeWidgetItem;

  vtkSMProxy* reprProxy = repr? repr->getProxy() : 0;
  if(!reprProxy)
    {
      return;
    }
  vtkObjectBase* obj = reprProxy->GetClientSideObject();

  itemError->setText(0,QString(obj->GetClassName()));
  // this->selTree->addTopLevelItem(itemError);

   vtkSMPropertyIterator* iter = reprProxy->NewPropertyIterator();
   for(iter->Begin(); !iter->IsAtEnd(); iter->Next())
     {
       vtkSMProperty* smProp = iter->GetProperty();
       //QTreeWidgetItem *item = new QTreeWidgetItem;
       //  item->setText(0,QString(smProp->GetXMLLabel()));
       //       this->selTree->addTopLevelItem(item);
     }

   /*
  vtkSMProperty* prop = reprProxy->GetProperty("SelectionCellFieldDataArrayName");
  itemError->setText(0,QString(prop->GetClassName()));
   this->selTree->addTopLevelItem(itemError);
   vtkSMStringVectorProperty* svp = NULL;
   svp = vtkSMStringVectorProperty::SafeDownCast(prop);
   int num = svp->GetNumberOfElements();
   std::stringstream std;
   std << num;
   itemError->setText(0,QString("%1 %2").arg(std.str().c_str(),svp->GetElement(0)));
   /*
  vtkSMIntVectorProperty* ivp = NULL;
  ivp = vtkSMIntVectorProperty::SafeDownCast(prop);
  int num = ivp->GetNumberOfElements();
   itemError->setText(0,QString("DowncastedIntVecProp"));
   this->selTree->addTopLevelItem(itemError);

  for(int i=0; i<num; i++)
    {
      bool ok = true;
      int v = ivp->GetElement(i);
      QTreeWidgetItem *itemlow = new QTreeWidgetItem;
      std::stringstream st;
      st << v;
      itemlow->setText(0,QString(st.str().c_str()));
      this->selTree->addTopLevelItem(itemlow);
    }

  // -------------------------------------------------

  /*
  vtkSMProxy* activeSelection = port->getSelectionInput();
  if(!activeSelection)
    {
      return;
    }

  vtkSelection* sel = vtkSelection::New();
  sel = vtkSelection::SafeDownCast(activeSelection->GetClientSideObject());

  QTreeWidgetItem *itemlow = new QTreeWidgetItem;
  std::stringstream st;
  //st << int1;


  if(!sel)
    {
      itemlow->setText(0,QString("Error"));
      this->selTree->addTopLevelItem(itemlow);
      return;
    }

  vtkFieldData* fd = sel->GetFieldData();
  


  // 123012 work
  /*
  pqView *view = 0;
  view = pqActiveObjects::instance().activeView();
  vtkView *clientView = view->getClientSideView();
  vtkPVView *pvView = vtkPVView::SafeDownCast(clientView);
  vtkPVRenderView *pvRenderView = vtkPVRenderView::SafeDownCast(clientView);
  vtkSelection *sel = pvRenderView->GetLastSelection();
  //
  vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::New();
  output = vtkMultiBlockDataSet::SafeDownCast(this->sourceProxy->GetClientSideObject());
  vtkExtractSelectedIds* ex = vtkExtractSelectedIds::New();
  ex->SetInputConnection(0,output);
  ex->SetInputConnection(1,sel);
  ex->Update();
  /*
  vtkUnstructuredGrid* extracted = vtkUnstructuredGrid::SafeDownCast(ex->GetOutput());
  vtkCellArray *cArray = extracted->GetCells();
  vtkIdTypeArray *idArray = cArray->GetData();
  double rang[2];
  idArray->GetRange(rang,0);
  QTreeWidgetItem *itemlow = new QTreeWidgetItem;
  std::stringstream st;
  st << rang[0];
  itemlow->setText(0,QString(st.str().c_str()));
  this->selTree->addTopLevelItem(itemlow);
  QTreeWidgetItem *itemhi = new QTreeWidgetItem;
  std::stringstream sr;
  sr << rang[1];
  itemhi->setText(0,QString(st.str().c_str()));
  this->selTree->addTopLevelItem(itemhi);


  vtkPointData *pd = extracted->GetPointData();
  vtkDataArray *da = vtkDataArray::SafeDownCast(pd->GetArray("EntityId"));
  vtkIdType idt = da->GetNumberOfTuples();
  for(int ii=0; ii < idt; ++ii)
    {
      double *d = new double;
      d = da->GetTuple(ii);
      QTreeWidgetItem *item = new QTreeWidgetItem;
      std::stringstream ss;
      ss << *d;
      item->setText(0,QString(ss.str().c_str()));
      this->selTree->addTopLevelItem(item);
    }



  if(da->GetNumberOfComponents() <= 0)
    {
      emit this->errorTag();
    }
  //  vtkPVInformation* pvInfo = vtkPVInformation*(dataInformation);
  //  vtkPVSelectionInformation* selPVInfo = vtkPVSelectionInformation::SafeDownCast(dataInformation);
  //vtkSelection* s = selPVInfo->GetSelection();

  Range selectionRange;

  if(!(this->sourceProxy))
    {
      return;
    }
  //*/


  // ----------------------/End/2013-01-03/----------------------

  //vtkMultiBlockDataSet *output = vtkMultiBlockDataSet::SafeDownCast(this->sourceProxy->GetClientSideObject());
  //  vtkSelection* s = vtkSelection::New();
  //vtkExtractSelectedIds* ex = vtkExtractSelectedIds::New();
  //ex->AddInputDataObject(0,output);
  //ex->AddInputDataObject(1,s);
  //  vtkFieldData* field = output->GetFieldData();
  /*vtkDataArray dArray = field->GetArray("EntityId");
  int numT = dArray->GetNumberofTuples();
  for(int i = 0; i < numT; ++i)
    {
      selectionRange.insert(dArray->GetComponent(i,0));
    }
  
  if(!(this->SelOutputPort))
    {
      return;
    }

  vtkPVDataInformation* dataInformation = NULL;
  if(this->SelOutputPort->getOutputPortProxy())
    {
      dataInformation = this->SelOutputPort->getDataInformation();
    }

  // if there isn't information, there's nothing we can do, so leave.
  if( !dataInformation)
    {
      return;
    }

  vtkPVDataSetAttributesInformation* info[3];
  info[0] = dataInformation->GetPointDataInformation();
  info[1] = dataInformation->GetCellDataInformation();
  info[2] = dataInformation->GetFieldDataInformation();

  for(int k=0; k<3; k++)
    {
      int numArrays = info[k]->GetNumberOfArrays();
      // Here we'll look through all the arrays until we find the Entity ID array.
      for(int i=0; i<numArrays; i++)
        {
          vtkPVArrayInformation* arrayInfo = info[k]->GetArrayInformation(i);
          QString arrayName = QString(arrayInfo->GetName());
          if( strcmp(arrayName.toStdString().c_str(),"EntityId") == 0 )
            {
              int numComponents = arrayInfo->GetNumberOfComponents();
              for(int j=0; j<numComponents; j++)
                {
                  // Do something here to get the entity handles in a range.
                }
            }
        }
    }
  //*/
}

void pqMoabReaderPanel::setupGUI()
{
  moab::ErrorCode rval;
  std::map<std::string, std::vector<std::string> > ehMap;

  // Grab the category tag values (names) off of the model and populate our selection mode list.
  rval = mbSel->LoadCatNameTags(this->MBI(),ehMap);
  mbSel->get_category_tags(ehMap, this->vecStr);
  unsigned int selModeVal = 1;
  for(std::vector<std::string>::iterator i = vecStr.begin(); i != vecStr.end(); ++i)
  {
    // Here we set the tag data to be the proper bit i.e. 1--1-- through QVariant data.
    selModeVal <<= 1;
    ++selModeVal;
    QVariant v(selModeVal);
    std::string string = *i;
    selectionMode->addItem(string.c_str(),v);
    --selModeVal;
  }

  // Bit Tag the MOAB entities (vertices is a misnomer).
  rval = mbSel->BitTagVertices(this->MBI(), vecStr);

  this->layout()->addWidget(selectionMode);


  // rearrange array selection lists
  headerItem->setText(0,QString("Category Values"));
  selTree->setHeaderItem(headerItem);
  /* The cat decided to code this section...
kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk     viiiiiiiiiiiiiiiiivvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
  */
  for(std::map<std::string, std::vector<std::string> >::iterator it = ehMap.begin(); it != ehMap.end(); ++it)
    {
      QTreeWidgetItem *itemKey = new QTreeWidgetItem();
      itemKey->setText(0,QString(it->first.c_str()));
      std::vector<std::string>::iterator iter;
      for(iter = it->second.begin(); iter != it->second.end(); ++iter)
        {
          QTreeWidgetItem *itemVal = new QTreeWidgetItem();
          itemVal->setText(0,QString((*iter).c_str()));
          itemKey->addChild(itemVal);
        }
      selTree->addTopLevelItem(itemKey); 
   
    }

  selTree->setFixedHeight(250);
  selTree->setSelectionMode(QAbstractItemView::ExtendedSelection);

  this->layout()->addWidget(selTree);
  //*/
  this->layout()->addWidget(addTreeCB);

  // Make the filter options
  std::vector<moab::Tag> vTag;
  rval = this->MBI()->tag_get_tags(vTag);
  for(std::vector<moab::Tag>::iterator ii = vTag.begin(); ii != vTag.end(); ++ii)
    {
      std::string tagName;
      rval = this->MBI()->tag_get_name(*ii,tagName);
      tagChoice->addItem(tagName.c_str());
    }
  tagChoice->addItem("New...");

  this->layout()->addWidget(tagChoice);

  // Add these to the layout, but make sure they're not visible...yet.
  this->layout()->addWidget(addTagLabel);
  newTagType->insertItem(0,"MB_TYPE_OPAQUE");
  newTagType->insertItem(1,"MB_TYPE_INTEGER");
  newTagType->insertItem(2,"MB_TYPE_DOUBLE");
  this->layout()->addWidget(newTagType);
  this->layout()->addWidget(newTagName);
  addTagLabel->setVisible(false);
  newTagName->setVisible(false);
  newTagType->setVisible(false);

  this->layout()->addWidget(new QLabel("Tag value", this));
  this->layout()->addWidget(value);
  this->layout()->addWidget(new QLabel("Entity handles", this));
  this->layout()->addWidget(eHandle);
  this->layout()->addWidget(applyBut);
  this->layout()->addWidget(delBut);
  this->layout()->addWidget(writeBut);

  // See if the user wants to make a new tag key and connect the slot
  QObject::connect(tagChoice,SIGNAL(currentIndexChanged(const QString)),this,SLOT(make_tag(QString)));

  // Connect the apply, write, and delete buttons to MOAB functions
  QObject::connect(applyBut,SIGNAL(clicked()),this,SLOT(add_tag()));
  QObject::connect(writeBut,SIGNAL(clicked()),this,SLOT(write_moab_file()));
  QObject::connect(delBut,SIGNAL(clicked()),this,SLOT(remove_tag()));

  // Add a new top level hierarchy to the tree
  QObject::connect(addTreeCB,SIGNAL(stateChanged(int)),this,SLOT(add_to_tree(int)));

  // Link selection in the tree to finding this entity range.
  QObject::connect(selTree,SIGNAL(itemSelectionChanged()),this,SLOT(getTreeSelectedEnts()));

  // Error in adding tag
  QObject::connect(this,SIGNAL(errorTag()),this,SLOT(popUpError()));
}

void pqMoabReaderPanel::updateGUI()
{
  // This only updates the Tag Key (Name) Combo Box.
  // There shouldn't be a need for anything else to be updated.
  // It doesn't make sense to alter the selection controls as that info isn't tagged
  // on the MOAB side. In the future, it could be useful to both update that and add
  // bit info onto that bit masking tag.
  moab::ErrorCode rval;

  tagChoice->clear();
  std::vector<moab::Tag> vTag;
  rval = this->MBI()->tag_get_tags(vTag);
  for(std::vector<moab::Tag>::iterator ii = vTag.begin(); ii != vTag.end(); ++ii)
    {
      std::string tagName;
      rval = this->MBI()->tag_get_name(*ii,tagName);
      if(rval == moab::MB_SUCCESS)
        {
          tagChoice->addItem(tagName.c_str());
        }
    }
  tagChoice->addItem("New...");

}

void pqMoabReaderPanel::getSelectionManager(pqSelectionManager *man)
{

}

pqOutputPort* pqMoabReaderPanel::getSelectionPort()
{
  pqSelectionManager* selMan = (pqSelectionManager*)(
                                                     pqApplicationCore::instance()->manager("SelectionManager"));
  if(!selMan)
    {
      qDebug() << "No selection manager detected. "
        " How do we get the active selection?";
      return NULL;
    }
  this->SelOutputPort = selMan->getSelectedPort();

  if(!this->SelOutputPort)
    {
      return NULL;
    }
  return this->SelOutputPort;
}

void pqMoabReaderPanel::getTreeSelectedEnts()
{
  this->selEnts.clear();
  QList<QTreeWidgetItem*> items = this->selTree->selectedItems();
  moab::EntityHandle eh;
  moab::ErrorCode rval;
  foreach(QTreeWidgetItem *item, items)
    {
      QString qStr = item->text(0);
      int entSetId = getQStrToEntitySetId(qStr);

      if(entSetId == 0)
        {
          continue;
        }
      rval = this->MBI()->handle_from_id(moab::MBENTITYSET,entSetId,eh);
      if(rval == moab::MB_SUCCESS)
        {
          this->selEnts.insert(eh);
        }
    }

}

int pqMoabReaderPanel::getEntitiesFromBox()
{
  moab::ErrorCode rval;
  QString eHandleText = this->eHandle->selectedText();
  if(eHandleText.isEmpty())
    {
      return 0;
    }

  bool ok;
  int entInt = eHandleText.toInt(&ok,10);
  if(!ok)
    {
      return 0;
    }

  moab::EntityHandle ent;
  rval = this->MBI()->handle_from_id(moab::MBVERTEX,entInt,ent);
  if(rval != moab::MB_SUCCESS)
    {
      return 0;
    }

  moab::Range preRange,endRange;
  preRange.insert(ent);

  QVariant var = this->selectionMode->itemData(this->selectionMode->currentIndex());
  unsigned int mode = var.toUInt(&ok);

  if(!ok)
    {
      return 0;
    }
  rval = this->mbSel->GetSelection(this->MBI(),mode,preRange,endRange,vecStr);
  this->selEnts.merge(endRange);
  return 1;
}

int pqMoabReaderPanel::getQStrToEntitySetId(QString qstring)
{
  std::string str = qstring.toStdString();
  int entSet = 0;

  //unsigned first = str.find("(");
  std::size_t begin = str.find_first_of("(");
  std::size_t end = str.find_last_of(")");

  if(std::string::npos != begin)
    {
      str.erase(end,1);
      str.erase(0, begin+1);
      entSet = atoi(str.c_str());
    }
  /*if(first == std::string::npos)
    {
      //unsigned last = str.find(")");
      std::string strNew = str.substr(first,last-first);
      entSet = atoi(strNew.c_str());
      }//*/
  else
    {
      entSet = atoi(str.c_str());
    }
  return entSet;
}

void pqMoabReaderPanel::make_tag(QString item)
{  
  if(item == QString("New..."))
    {
      addTagLabel->setVisible(true);
      newTagName->setVisible(true);
      newTagType->setVisible(true);
    }
  else
    {
      addTagLabel->setVisible(false);
      newTagName->setVisible(false);
      newTagType->setVisible(false);
    }
}

void pqMoabReaderPanel::add_tag()
{
  moab::ErrorCode rval;

  // Get the tag the user wants to add or create it
  moab::Tag addTag;
  QString curVal = value->text();
  bool ok;

  if(this->selEnts.empty())
    {
      this->getEntitiesFromBox();
      //this->extractEntityHandleSelection();
    }

  QString curItem = tagChoice->currentText();
  if(curItem == "New...")
    {
      curItem = newTagName->text();
      int dtype = newTagType->currentIndex();

      if(dtype == moab::MB_TYPE_OPAQUE)
        {
          rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_OPAQUE,addTag,moab::MB_TAG_CREAT|moab::MB_TAG_SPARSE);

          std::string tagStr = curVal.toStdString();
          char *charTag_data = new char[32];
          //          char *charTag_data = ba.c_str();
          //              *charTag_data = tagStr.append("\0").c_str();
          strcpy(charTag_data,tagStr.c_str());
          for(moab::Range::iterator ii = this->selEnts.begin(); ii != this->selEnts.end(); ++ii)
            {
              rval = this->MBI()->tag_set_data(addTag,&*ii,1,charTag_data);
            }
          value->setText(QString(*charTag_data));
          delete[] charTag_data;
        }
      else if(dtype == moab::MB_TYPE_INTEGER)
        {
          rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_INTEGER,addTag,moab::MB_TAG_CREAT|moab::MB_TAG_SPARSE);

          int *intTag_data = new int;
          *intTag_data = curVal.toInt(&ok,10);
          if(ok == false)
            {
              emit this->errorTag();
              return;
            }
          for(moab::Range::iterator ii = this->selEnts.begin(); ii != this->selEnts.end(); ++ii)
            {
              rval = this->MBI()->tag_set_data(addTag,&*ii,1,intTag_data);
            }
          std::stringstream ss;
          ss << *intTag_data;
          value->setText(QString(ss.str().c_str()));
        }
      else if(dtype == moab::MB_TYPE_DOUBLE)
        {
          rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_DOUBLE,addTag,moab::MB_TAG_CREAT|moab::MB_TAG_SPARSE);

          double *doubleTag_data = new double;
          *doubleTag_data = curVal.toDouble(&ok);
          if(ok == false)
            {
              emit this->errorTag();
              return;
            }
          for(moab::Range::iterator ii = this->selEnts.begin(); ii != this->selEnts.end(); ++ii)
            {
              rval = this->MBI()->tag_set_data(addTag,&*ii,1,doubleTag_data);
            }

          std::stringstream ss;
          ss << *doubleTag_data;
          value->setText(QString(ss.str().c_str()));
        }
      else
        {
          emit this->errorTag();
        } 
     this->updateGUI();
    } 
  else
    {
      rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),addTag);

      moab::TagType ttype;
      moab::DataType dtype;
      rval = this->MBI()->tag_get_type(addTag,ttype);
      rval = this->MBI()->tag_get_data_type(addTag,dtype);

      if(dtype == moab::MB_TYPE_INTEGER)
        {
          int *intTag_data = new int;
          *intTag_data = curVal.toInt(&ok,10);
          for(moab::Range::iterator ii = this->selEnts.begin(); ii != this->selEnts.end(); ++ii)
            {
              rval = this->MBI()->tag_set_data(addTag,&*ii,1,intTag_data);
            }

          std::stringstream ss;
          ss << *intTag_data;
          value->setText(QString(ss.str().c_str()));

        }
      else if(dtype == moab::MB_TYPE_DOUBLE)
        {
          double *doubleTag_data = new double;
          *doubleTag_data = curVal.toDouble(&ok);
          for(moab::Range::iterator ii = this->selEnts.begin(); ii != this->selEnts.end(); ++ii)
            {
              rval = this->MBI()->tag_set_data(addTag,&*ii,1,doubleTag_data);
            }

          std::stringstream ss;
          ss << *doubleTag_data;
          value->setText(QString(ss.str().c_str()));
        }
      else if(dtype == moab::MB_TYPE_OPAQUE)
        {
          std::string tagStr = curVal.toStdString();
          char *charTag_data = new char[32];
          // *charTag_data = tagStr.append("\0").c_str()
          strcpy(charTag_data,tagStr.c_str());
          // char *charTag_data = ba.c_str();
          for(moab::Range::iterator ii = this->selEnts.begin(); ii != this->selEnts.end(); ++ii)
            {
              rval = this->MBI()->tag_set_data(addTag,&*ii,1,charTag_data);
            }

          value->setText(QString(*charTag_data));
          delete[] charTag_data;
        }
      else
        {
          emit this->errorTag();
          return;
        }
    }
 
  emit this->applyTag();
}

void pqMoabReaderPanel::add_to_tree(int state)
{
  if(state != 2)
    {
      return;
    }
  moab::Tag addTag;
  moab::Range result;
  moab::ErrorCode rval;
  int val = 0;
  QString curItem = tagChoice->currentText();
  // Get the tag the user wants to add
  rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_INTEGER,addTag);

  if(rval != moab::MB_SUCCESS)
    {
      rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_DOUBLE,addTag);
      val = 1;
      if(rval != moab::MB_SUCCESS)
        {
          rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_OPAQUE,addTag);
          val = 2;
        }
    }
  // Let's get all the entity sets that have this tag
  this-MBI()->get_entities_by_type_and_tag(0,moab::MBENTITYSET,&addTag,NULL,1,result,moab::Interface::UNION);

  QList<QTreeWidgetItem*> topLevels = this->selTree->findItems(curItem,Qt::MatchWrap|Qt::MatchRecursive|Qt::MatchFixedString);
  QTreeWidgetItem *itemKey = new QTreeWidgetItem();
  if(topLevels.empty())
    {
      itemKey->setText(0,tagChoice->currentText());
    }
  else
    {
      itemKey = topLevels[0];
    }

  int bytes;
  this->MBI()->tag_get_bytes(addTag,bytes);
  char *lowTree1 = new char[32];
  int * lowTreeInt = new int;
  double * lowTreeDouble = new double;

  std::string tagName;
  //  rval = this->MBI()->tag_get_name(addTag,tagName);
  for(moab::Range::iterator ii = result.begin(); ii != result.end(); ++ii)
    {
      moab::EntityHandle eh = *ii;
      QList<QTreeWidgetItem*> listofitems;
      QTreeWidgetItem *itemVal = new QTreeWidgetItem();

      if(val == 0)
        {
          this->MBI()->tag_get_data(addTag,&eh,1,lowTreeInt);
          std::stringstream ss;
          ss << *lowTreeInt;
          itemVal->setText(0,QString(ss.str().c_str()));
          listofitems = this->selTree->findItems(QString(ss.str().c_str()),Qt::MatchWrap|Qt::MatchRecursive|Qt::MatchFixedString);
        }
      else if(val == 1)
        {
          this->MBI()->tag_get_data(addTag,&eh,1,lowTreeDouble);
          std::stringstream ss;
          ss << *lowTreeDouble;
          itemVal->setText(0,QString(ss.str().c_str()));
          listofitems = this->selTree->findItems(QString(ss.str().c_str()),Qt::MatchWrap|Qt::MatchRecursive|Qt::MatchFixedString);
        }
      else if(val == 2)
        {
          this->MBI()->tag_get_data(addTag,&eh,1,lowTree1);
          itemVal->setText(0,QString(*lowTree1));
          listofitems = this->selTree->findItems(QString(*lowTree1),Qt::MatchWrap|Qt::MatchRecursive|Qt::MatchFixedString);
        }
      else
        {
          return;
        }
      if(listofitems.empty())
        {
          QTreeWidgetItem* itemSets = new QTreeWidgetItem();
          std::stringstream ss;
          ss << this->MBI()->id_from_handle(eh);
          itemSets->setText(0,QString(ss.str().c_str()));
          itemVal->addChild(itemSets);
          itemKey->addChild(itemVal);
        }
      else
        {
          QTreeWidgetItem* itemSets = new QTreeWidgetItem();
          std::stringstream ss;
          ss << this->MBI()->id_from_handle(eh);
          itemSets->setText(0,QString(ss.str().c_str()));
          listofitems[0]->addChild(itemSets);
        }
      /*
          moab::Range subEntSets;
          this->MBI()->get_entities_by_type(eh,moab::MBENTITYSET,subEntSets);
          for(moab::Range::iterator j = subEntSets.begin(); j != subEntSets.end(); ++j)
            {
              std::stringstream ss;
              ss << this->MBI()->id_from_handle(*j);
              std::string sId = ss.str();
              QTreeWidgetItem* itemSets = new QTreeWidgetItem();
              itemSets->setText(0,QString(sId.c_str()));
              itemVal->addChild(itemSets);
            }
          //*/
      //itemKey->addChild(itemVal);
     }
   selTree->addTopLevelItem(itemKey); 
   this->addTreeCB->setCheckState(Qt::Unchecked);
   delete[] lowTree1;
}

void pqMoabReaderPanel::remove_tag()
{
  moab::ErrorCode rval;

  QString curItem = tagChoice->currentText();
  if(curItem == "New...")
    {
      curItem = newTagName->text();
    } 

  // Remove the tag from the hierarchy if it's there

  QList<QTreeWidgetItem*> listofitems = this->selTree->findItems(QString(curItem.toStdString().c_str()),Qt::MatchExactly);
  if(!listofitems.empty())
    {
      foreach(QTreeWidgetItem *item, listofitems)
        {
          this->selTree->removeItemWidget(item,0);
        }
    }


  // Get the tag the user wants to add or create it
  moab::Tag addTag;
  QString curVal = value->text();
  bool ok;
  int *intTag_data = new int;
  *intTag_data = curVal.toInt(&ok,10);

  if(ok == true)
    {
      rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_INTEGER,addTag,MB_TAG_CREAT);
      rval = this->MBI()->tag_delete_data(addTag,this->selEnts);
    }
  else
    {
      double *doubleTag_data = new double;
      *doubleTag_data = curVal.toDouble(&ok);

      if(ok == true)
        {
          rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_DOUBLE,addTag,MB_TAG_CREAT);
          rval = this->MBI()->tag_delete_data(addTag,this->selEnts);
        }
      else
        {
          rval = this->MBI()->tag_get_handle(curItem.toStdString().c_str(),1,moab::MB_TYPE_OPAQUE,addTag,MB_TAG_CREAT);
          rval = this->MBI()->tag_delete_data(addTag,this->selEnts);
        }
    }
}

void pqMoabReaderPanel::write_moab_file()
{
  moab::ErrorCode rval;
  rval = this->MBI()->write_file( this->fn);
}

void pqMoabReaderPanel::popUpError()
{
  QMessageBox messageBox;
  messageBox.setText("This tag is not of the right type.");
  messageBox.exec();
}
