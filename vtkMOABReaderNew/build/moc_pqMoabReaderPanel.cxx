/****************************************************************************
** Meta object code from reading C++ file 'pqMoabReaderPanel.h'
**
** Created: Tue Jan 21 14:02:30 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../pqMoabReaderPanel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pqMoabReaderPanel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_pqMoabReaderPanel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x05,
      30,   18,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      45,   41,   18,   18, 0x09,
      63,   18,   18,   18, 0x09,
      73,   18,   18,   18, 0x09,
      97,   91,   18,   18, 0x09,
     114,   18,   18,   18, 0x09,
     127,   18,   18,   18, 0x09,
     149,   18,   18,   18, 0x09,
     174,  162,   18,   18, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_pqMoabReaderPanel[] = {
    "pqMoabReaderPanel\0\0applyTag()\0errorTag()\0"
    "tag\0make_tag(QString)\0add_tag()\0"
    "write_moab_file()\0state\0add_to_tree(int)\0"
    "remove_tag()\0getTreeSelectedEnts()\0"
    "popUpError()\0,,,callData\0"
    "selectionChanged(vtkObject*,ulong,void*,void*)\0"
};

void pqMoabReaderPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        pqMoabReaderPanel *_t = static_cast<pqMoabReaderPanel *>(_o);
        switch (_id) {
        case 0: _t->applyTag(); break;
        case 1: _t->errorTag(); break;
        case 2: _t->make_tag((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->add_tag(); break;
        case 4: _t->write_moab_file(); break;
        case 5: _t->add_to_tree((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->remove_tag(); break;
        case 7: _t->getTreeSelectedEnts(); break;
        case 8: _t->popUpError(); break;
        case 9: _t->selectionChanged((*reinterpret_cast< vtkObject*(*)>(_a[1])),(*reinterpret_cast< ulong(*)>(_a[2])),(*reinterpret_cast< void*(*)>(_a[3])),(*reinterpret_cast< void*(*)>(_a[4]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData pqMoabReaderPanel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject pqMoabReaderPanel::staticMetaObject = {
    { &pqLoadedFormObjectPanel::staticMetaObject, qt_meta_stringdata_pqMoabReaderPanel,
      qt_meta_data_pqMoabReaderPanel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &pqMoabReaderPanel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *pqMoabReaderPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *pqMoabReaderPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_pqMoabReaderPanel))
        return static_cast<void*>(const_cast< pqMoabReaderPanel*>(this));
    return pqLoadedFormObjectPanel::qt_metacast(_clname);
}

int pqMoabReaderPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = pqLoadedFormObjectPanel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void pqMoabReaderPanel::applyTag()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void pqMoabReaderPanel::errorTag()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
