#ifndef VTKMOABREADER_H
#define VTKMOABREADER_H

#include "vtkIOGeometryModule.h" // For export macro
#include "vtkMultiBlockDataSetAlgorithm.h"
#include "vtkNew.h" //needed for api signature
//#include "../src/MoabParaViewInterface.hpp"
#include "vtkDataArraySelection.h"
#include "vtkStringArray.h"
#include "moab/Core.hpp"

//moab::Interface *MOAB();

class vtkInformation;
class vtkInformationVector;
class Core;

namespace smoab{ class Tag; class Interface; }


class vtkMoabReader : public vtkMultiBlockDataSetAlgorithm//, public virtual MoabParaViewInterface
{
public:

  std::vector<std::string> SelFilterInfo;
  std::vector<std::string> SelFilter;
  static moab::Core instance;
  moab::Interface *Moab();
  static vtkMoabReader *New();
  vtkTypeMacro(vtkMoabReader,vtkMultiBlockDataSetAlgorithm)
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Specify file name of the MOAB mesh file.
  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);


protected:
  vtkMoabReader();
  ~vtkMoabReader();

  int FillOutputPortInformation(int port, vtkInformation* info);

  int RequestInformation(vtkInformation *vtkNotUsed(request),
                         vtkInformationVector **vtkNotUsed(inputVector),
                         vtkInformationVector *outputVector);

  int RequestData(vtkInformation *vtkNotUsed(request),
                  vtkInformationVector **vtkNotUsed(inputVector),
                  vtkInformationVector *outputVector);
private:
  void CreateSubBlocks(vtkNew<vtkMultiBlockDataSet> &root,
                       smoab::Interface* interface,
                       smoab::Tag const* parentTag,
                       smoab::Tag const* extractTag=NULL);

  void CreateGeomBlocks(vtkNew<vtkMultiBlockDataSet> &root,
                        smoab::Interface* interface,
                        smoab::Tag const* t);

  void CreateTagBlocks(vtkNew<vtkMultiBlockDataSet> & root,
                       smoab::Interface* interface,
                       smoab::Tag const* t, std::string str);

  vtkMoabReader(const vtkMoabReader&);  // Not implemented.
  void operator=(const vtkMoabReader&);  // Not implemented.
  char* FileName;

  //std::vector<std::string> GetSelFilterList(){
  //return this->SelFilterInfo;};
  //std::vector<std::string> SetSelFilter(std::string str){this->SelFilter.clear();this->SelFilter.push_back(str); return this->SelFilter;};
};


#endif // VTKMOABREADER_H
