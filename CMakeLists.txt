project( VTKMOABPANEL )

cmake_minimum_required(VERSION 2.8.2 FATAL_ERROR)

find_library(MOAB_LIBRARY MOAB REQUIRED)

find_path(MOAB_INCLUDE_DIRS Core.hpp REQUIRED)
include_directories(${MOAB_INCLUDE_DIRS})

set(CMAKE_CXX_FLAG "-g -Wall")

add_subdirectory(SelectionAlgorithm)


