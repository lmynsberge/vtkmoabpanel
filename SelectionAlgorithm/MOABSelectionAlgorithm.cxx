#include "MOABSelectionAlgorithm.h"

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <stdio.h>
#include <sstream>

#include "MBTagConventions.hpp"
#include "moab/CartVect.hpp"
#include "moab/Range.hpp"
#include "moab/Core.hpp"
#include "moab/GeomUtil.hpp"
#include "moab/Core.hpp"
#include "moab/Interface.hpp"

#define BIT_TAG_NAME "Bit Tag"

using namespace moab;

//------------------------------------------------------------------------------
MOABSelectionAlgorithm::MOABSelectionAlgorithm()
{
  //this->Moab = new moab::Core();
  this->BitTagVertex = false;
}

//------------------------------------------------------------------------------
MOABSelectionAlgorithm::~MOABSelectionAlgorithm()
{
  if(this->Moab)
    {
      delete this->Moab;
      this->Moab = NULL;
    }
}

//! Loads category tag values into a map structure and adds the entity sets contained within each
/** 
Given a Moab interface, the function will get a map of strings that have a "key" of the category tag value and a "value" of the contained entity set name and/or global id.
\param mb : The Moab database to fetch the information from
\param eMap : The map to return the tree information to.

Example: \code
\pending
 */
ErrorCode MOABSelectionAlgorithm::LoadCatNameTags(moab::Interface *mb, std::map<std::string, std::vector<std::string> > &eMap)
{
  int id;
  std::string sId;
  ErrorCode rval;
  Tag catTag, nameTag;
  Range entsWTag;
  std::string str;
  long unsigned int *bitTagSpace = new long unsigned int;
  Range verts;
  Tag bitTag;
  const long unsigned int default_val = 0;
  long unsigned int bitTagValue = 0;
  *bitTagSpace = bitTagValue;
  std::vector<std::string> vStr;
  int noNameTag = 0;

  // Get Category Tags
  rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, catTag);
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &catTag, NULL, 1, entsWTag, 
          Interface::UNION);
  if(rval != MB_SUCCESS)
    {
      return rval;
    }
  // Get Name Tags to grab names of individual category entity sets
  rval = mb->tag_get_handle(NAME_TAG_NAME, NAME_TAG_SIZE, MB_TYPE_OPAQUE, nameTag);
  if(rval == MB_SUCCESS)
    {
      noNameTag = 1;
    }

  // Begin the loop to create the "filter" groups and tree.
  // Optionally: tag all entities the first time around.
  char *tagCat_data = new char[CATEGORY_TAG_SIZE];
  char *tagName_data = new char[NAME_TAG_SIZE];
  int Cexists = 0;
  int Nexists = 0;
  std::string entName;
  std::vector<std::string> catTree;
  std::string cTagData,nTagData;

  for (Range::iterator i = entsWTag.begin(); i != entsWTag.end(); ++i)
    {
      EntityHandle eh = *i;
      std::vector<std::string>::iterator iter;

      // Get current EntityHandle Global ID and Category/Name Tag Data and see if it exists.
      // If it doesn't exist, add it to the vector of strings for large groups.
      id = mb->id_from_handle(eh);
      std::stringstream ss;
      ss << id;
      sId = ss.str();
      rval = mb->tag_get_data(catTag,&eh,1,tagCat_data);
      cTagData = std::string(tagCat_data);

      if(noNameTag == 1)
        {
          rval = mb->tag_get_data(nameTag,&eh,1,tagName_data);
        }

      std::string prevTag;
      if(rval == MB_SUCCESS)
        {
          nTagData = std::string(tagName_data);
          entName = nTagData.append(" (").append(sId).append(")");
        }
      else
        {
          entName = " (";
          entName.append(sId).append(")");
        }


      // Get current EntityHandle Global ID and Category Tag Data and see if it exists.
      // If it doesn't exist, add it to the vector of strings for large groups.
      if( vStr.empty() )
        {
          vStr.push_back(cTagData);
        }


      for(iter = vStr.begin(); iter != vStr.end(); ++iter)
        {
          if( (cTagData.compare(*iter)==0) )
            {
              Cexists = 1;
              catTree.push_back(entName);
            }

        }

      if( Cexists == 0 )
        {
          std::vector<std::string>::iterator it = find(vStr.begin(), vStr.end(),cTagData);
          --it;
          std::string tmpStr = *it;
          eMap[tmpStr] = catTree;
          vStr.push_back(cTagData);
          catTree.clear();
        }


      Cexists = 0;
    }


  if(entsWTag.size() != 0)
    {
    std::vector<std::string>::iterator it = find(vStr.begin(), vStr.end(),cTagData);
    std::string tmpStr = *it;
    eMap[tmpStr] = catTree;
    vStr.push_back(cTagData);
    }

  return rval;
}

void MOABSelectionAlgorithm::get_category_tags(std::map<std::string, std::vector<std::string> > eMap, std::vector<std::string> &vStr)
{
  std::map<std::string, std::vector<std::string> >::iterator iter;
  for(iter = eMap.begin(); iter != eMap.end(); ++iter)
    {
      vStr.push_back(iter->first);
    }
}

ErrorCode MOABSelectionAlgorithm::ListChildren(moab::Interface *mb, std::string parentName, std::map<std::string, std::vector<std::string> > &children)
{
  ErrorCode rval;

  // New stuff.
  Tag parentTag, nameTag;
  Range groups;
  int id;

  std::string sId,tagVal;
  rval = mb->tag_get_handle(parentName.c_str(),parentTag);
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &parentTag, NULL, 1, groups, 
          Interface::UNION);
  if(rval == MB_TAG_NOT_FOUND)
    {
      return rval;
    }
  DataType dt;
  rval = mb->tag_get_data_type(parentTag,dt);

  std::string entName;
  std::string cTagData,nTagData;

  for (Range::iterator i = groups.begin(); i != groups.end(); ++i)
    {
      EntityHandle eh = *i;
      std::vector<std::string>::iterator iter;

      // Get current EntityHandle Global ID and Category/Name Tag Data and see if it exists.
      // If it doesn't exist, add it to the vector of strings for large groups.
      id = mb->id_from_handle(eh);
      std::stringstream ss;
      ss << id;
      sId = ss.str();

      if(dt == MB_TYPE_INTEGER)
        {
          int * data = new int;
          rval = mb->tag_get_data(parentTag,&eh,1,data);
          std::stringstream st;
          st << data;
          tagVal = st.str();
        }
      if(dt == MB_TYPE_DOUBLE)
        {
          double * data = new double;
          rval = mb->tag_get_data(parentTag,&eh,1,data);
          std::stringstream st;
          st << data;
          tagVal = st.str();
        }
      if(dt == MB_TYPE_OPAQUE)
        {
          char * data = new char[CATEGORY_TAG_SIZE];
          rval = mb->tag_get_data(parentTag,&eh,1,data);
          std::stringstream st;
          st << data;
          tagVal = st.str();
        }
      else
        {
          return rval;
        }

      rval = mb->tag_get_handle(NAME_TAG_NAME, NAME_TAG_SIZE, MB_TYPE_OPAQUE, nameTag);
      char *tagName_data = new char[NAME_TAG_SIZE];
      rval = mb->tag_get_data(nameTag,&eh,1,tagName_data);
      if(rval == MB_SUCCESS)
        {
          nTagData = std::string(tagName_data);
          entName = nTagData.append(" (").append(sId).append(")");
        }
      else
        {
          entName = " (";
          entName.append(sId).append(")");
        }

      children[tagVal].push_back(entName);
    }
  // Old stuff.
  /*
  int id;
  Tag category_tag, name_tag;
  rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, category_tag);
  char group_category[CATEGORY_TAG_SIZE];
  std::fill(group_category, group_category+CATEGORY_TAG_SIZE, '\0');
  sprintf(group_category, "%s", parentName.c_str());
  const void* const group_val[] = {&group_category};
  Range groups;
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &category_tag, group_val, 1, groups);

  char tagName_data[NAME_TAG_SIZE];
  rval = mb->tag_get_handle(NAME_TAG_NAME, NAME_TAG_SIZE, MB_TYPE_OPAQUE, name_tag);

  std::string idStr;
  for(Range::iterator it=groups.begin(); it != groups.end(); ++it)
    {
      id = mb->id_from_handle(*it);
      idStr = std::string(idStr);
      children.push_back(idStr);
    }
  if(children.empty())
    {
  std::fill(group_category, group_category+CATEGORY_TAG_SIZE, '\0');
  sprintf(group_category, "%s", parentName.c_str());
  const void* const group_val[] = {&group_category};
  Range groups;
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &name_tag, group_val, 1, groups);
  std::string idStr;
  for(Range::iterator it=groups.begin(); it != groups.end(); ++it)
    {
      id = mb->id_from_handle(*it);
      idStr = std::string(idStr);
      children.push_back(idStr);
    }

    }
  //*/
  return rval;
}

ErrorCode MOABSelectionAlgorithm::GetSelection(moab::Interface *mb, long unsigned int selMode, Range selIds, Range &results, std::vector<std::string> vStr)
{
  ErrorCode rval;
  Tag bitTag;
  long unsigned int *bitTagSpace = new long unsigned int;
  Tag tag;
  Range allEnts;
  long unsigned int sMode = selMode;
  std::map<std::string, std::vector<std::string> >::iterator iter;


  rval = mb->tag_get_handle(BIT_TAG_NAME,1,MB_TYPE_INTEGER, bitTag, MB_TAG_SPARSE);

  int shift = 0;
  while( selMode%2 == 0 )
    {
      shift += 1;
      selMode >>= 1;
    }
  int remains = vStr.size() - shift;

  std::string selString = vStr[remains-1];
  std::cout << "category is : " << selString << "\n";
  std::cout << "shift is: " << shift << " remains is: " << remains << "\n";
  // First let's get the division of category that we're in.
  rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, tag);
  char charIndex[CATEGORY_TAG_SIZE];
  std::fill(charIndex, charIndex+CATEGORY_TAG_SIZE, '\0');
  sprintf(charIndex, "%s", selString.c_str());
  const void* const group_val[] = {&charIndex};
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &tag, group_val, 1, allEnts, Interface::UNION);

  //  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &tag, NULL, 1, allEnts,Interface::UNION);
  Range ents;
  for(Range::iterator i = allEnts.begin(); i != allEnts.end(); ++i)
    {
      EntityHandle eh = *i;
      mb->get_entities_by_handle(eh,ents);
      if(selIds.size() <= ents.size())
        {
          for(Range::iterator k = selIds.begin(); k != selIds.end(); ++k)
            {
              EntityHandle kEh = *k;
              Range test;
              test.insert(kEh);
              if(ents.contains(test))
                {
                  results.insert(eh);
                }
              test.clear();
            }
        }
      else if (selIds.size() > ents.size() )
        {
          for(Range::iterator k = ents.begin(); k != ents.end(); ++k )
            {
              EntityHandle kEh = *k;
              Range test;
              test.insert(kEh);
              if(selIds.contains(test))
                {
                  results.insert(eh);
                }
              test.clear();
            }
        }
    }

  return rval;
}

void MOABSelectionAlgorithm::FindOptimalEntity(moab::Interface *mb, std::vector<Range> vRange, EntityHandle &optEntity)
{
  std::vector<Range>::iterator i;
  std::vector<EntityHandle> vEh;

  for(i = vRange.begin(); i != vRange.end(); ++i)
    {
      Range cur = *i;
      if(cur.size() > 1)
        {
          for(Range::iterator j = cur.begin(); j != cur.end(); ++j)
            {
              vEh.push_back(*j);
            }
          continue;
        }
      vEh.push_back(cur.front());
    }
  optEntity = this->MostCommonEnt(vEh);
}

EntityHandle MOABSelectionAlgorithm::MostCommonEnt(std::vector<EntityHandle> &vEh)
{
  std::map<EntityHandle, int> ehMap;
  std::vector<EntityHandle>::iterator i;
  for(i = vEh.begin(); i != vEh.end(); ++i)
    {
      ehMap[*i] = 0;
    }
  for(i = vEh.begin(); i != vEh.end(); ++i)
    {
      ehMap[*i] += 1;
    }
  typedef std::map<EntityHandle, int>::iterator mIter;
  mIter it = ehMap.begin();
  mIter end = ehMap.end();

  int max_value = it->second;
  EntityHandle optEntity = it->first;
  for( ; it != end; ++it)
    {
      if(it->second > max_value)
        {
          max_value = it->second;
          optEntity = it->first;
        }
    }

  return optEntity;
}

//----------------------------------------------------------
ErrorCode MOABSelectionAlgorithm::BitTagVertices(moab::Interface *mb, std::vector<std::string> vStr)
{
  ErrorCode rval;

  Tag bitTag,catTag;
  Range entSets, allEnts;

  long unsigned int *bitTag_data = new long unsigned int;
  long unsigned int default_val = 1;
  *bitTag_data = 1;

  // Get all category sets.
  rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, catTag);
  
  /* In the future, right here will check the number of values
  on the set before creating the bit tag, in case it exceeds 
  the unsigned long int maximum of 64 differing categories
  //*/
  
  // Create bit tag information
  rval = mb->tag_get_handle(BIT_TAG_NAME,1,MB_TYPE_INTEGER, bitTag, MB_TAG_CREAT|MB_TAG_DENSE);
  
  // Initialize all entities' bit tags.
  rval = mb->get_entities_by_handle(0,allEnts);
  for(Range::iterator i = allEnts.begin(); i != allEnts.end(); ++i)
  {
    rval = mb->tag_set_data(bitTag,&*i,1,&default_val);
  }
  
  for(std::vector<std::string>::iterator it = vStr.begin(); it != vStr.end(); ++it)
    {
      std::string curStr = *it;
      char charIndex[CATEGORY_TAG_SIZE];
      std::fill(charIndex, charIndex+CATEGORY_TAG_SIZE, '\0');
      sprintf(charIndex, "%s", curStr.c_str());
      const void* const group_val[] = {&charIndex};
      rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &catTag, group_val, 1, entSets, 
          Interface::UNION);

      Range entList,ents;
      for(Range::iterator i = entSets.begin(); i != entSets.end(); ++i)
        {
          EntityHandle eh = *i;
          rval = mb->get_entities_by_handle(eh,ents);
          entList.merge(ents);
          ents.clear();
        }
	
	// Let's now category bit mask every tag in our range (this will need to be done at the end, too).
      for(Range::iterator j = allEnts.begin(); j != allEnts.end(); ++j)
        {         
          EntityHandle jEh = *j;
          Range entRange;
          rval = mb->tag_get_data(bitTag,&jEh,1,bitTag_data);
	  *bitTag_data <<= 1;
	  entRange.insert(jEh);
          if(entList.contains(entRange))
            {
              *bitTag_data += 1;
            }
	  rval = mb->tag_set_data(bitTag,&jEh,1,bitTag_data);
          entRange.clear();
	}
	// Clear entList, so the next set of entities contained in entity sets with the same value can be tagged.
      entList.clear();
      entSets.clear();
    }
  return rval;
}








