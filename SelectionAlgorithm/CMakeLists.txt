include_directories(${MOABALGO_SOURCE_DIR}/SelectionAlgorithm)
link_directories(${MOABALGO_BINARY_DIR}/SelectionAlgorithm)
include_directories(${MOAB_INCLUDE_DIRS})

add_library(MOABSelectionAlgorithm MOABSelectionAlgorithm.cxx)

add_executable(AlgorithmTest AlgorithmTest.cxx)
target_link_libraries(AlgorithmTest ${MOAB_LIBRARY} MOABSelectionAlgorithm)




