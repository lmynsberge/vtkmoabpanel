#include "MOABSelectionAlgorithm.h"

#include <iostream>
#include <string>

#include "MBTagConventions.hpp"
#include "moab/CartVect.hpp"
#include "moab/Range.hpp"
#include "moab/Core.hpp"
#include "moab/GeomUtil.hpp"
#include "moab/Core.hpp"
#include "moab/Interface.hpp"

#define HIERARCHY_TAG_NAME "HIERARCHY TAG"

using namespace moab;

//------------------------------------------------------------------------------
MOABSelectionAlgorithm::MOABSelectionAlgorithm()
{
  this->Moab = new Core();
}

//------------------------------------------------------------------------------
MOABSelectionAlgorithm::~MOABSelectionAlgorithm()
{
  if(this->Moab)
    {
      delete this->Moab;
      this->Moab = NULL;
    }
}

//------------------------------------------------------------------------------
ErrorCode MOABSelectionAlgorithm::LoadInformation(moab::Interface *mb)
{
  // Let's grab the 
  EntityHandle eh = mb->get_root_set();

  int id;
  Range entSetsRange;
  ErrorCode rval;
  rval = mb->get_entities_by_type( 0, MBENTITYSET, entSetsRange);
  Tag tag;
  Range entsWTag;
  std::string str;
  std::vector<std::string> vStr;

  // Get Category Tags
  rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, tag);
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &tag, NULL, 1, entsWTag, 
          Interface::UNION);
  std::cout << "Category Tags: \n";
  char tag_data[CATEGORY_TAG_SIZE];
  std::vector<std::string>::iterator iter;
  for (Range::iterator i = entsWTag.begin(); i != entsWTag.end(); ++i)
    {
      id = mb->id_from_handle(*i);
      rval = mb->tag_get_data(tag,&*i,1,&tag_data);
      std::string sTagData = std::string(tag_data);
      iter = std::find(vStr.begin(),vStr.end(),sTagData);
      std::string iter2 = *iter;
      if( iter2.compare(sTagData) && sTagData.compare(*(vStr.end())))
        {
          vStr.push_back(std::string(tag_data));
        }
      std::vector<Tag> tagGroup;
      rval = mb->tag_get_tags_on_entity(*i,tagGroup);
      for(std::vector<Tag>::iterator i2 = tagGroup.begin(); i2 != tagGroup.end(); ++i2)
        {
          rval = mb->tag_get_name(*i2,str);
          std::cout << "--> " << id << "Data: " << tag_data << "Name: " << str << "\n";
        }
    }

  // Get Name Tags
  char tagName_data[NAME_TAG_SIZE];
  rval = mb->tag_get_handle(NAME_TAG_NAME, NAME_TAG_SIZE, MB_TYPE_OPAQUE, tag);
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &tag, NULL, 1, entsWTag, 
          Interface::INTERSECT);
  std::cout << "Name Tags: \n";
  for (Range::iterator i = entsWTag.begin(); i != entsWTag.end(); ++i)
    {
      id = mb->id_from_handle(*i);
      rval = mb->tag_get_data(tag,&*i,1,&tagName_data);
      std::string sTagNameData = std::string(tagName_data);
      iter = std::find(vStr.begin(),vStr.end(),sTagNameData);
      std::string iter2 = *iter;
      if( iter2.compare(sTagNameData) && sTagNameData.compare(*(vStr.end())))
        {
          vStr.push_back(std::string(tagName_data));
        }
      std::cout << "--> " << id << "Name: " << tagName_data << "\n";
    }

  // List all highest level tags
  std::vector<Tag> tag_handles;
  rval = mb->tag_get_tags(tag_handles);
  for (std::vector<Tag>::iterator it = tag_handles.begin(); it != tag_handles.end(); ++it)
    {
      rval = mb->tag_get_name(*it,str);
      std::cout << "Tags on mesh: " << str << "\n";
    }

  Range::iterator i; 
  
  for (i=entSetsRange.begin(); i != entSetsRange.end(); ++i)
    {
      std::vector<EntityHandle> children;
      rval = mb->get_child_meshsets(*i,children); 
      if (children.empty())
        {
          continue;
        }
      id = mb->id_from_handle(*i);
  
      std::cout << "Entity Set Id: " << id << "\n";
     
     for(std::vector<EntityHandle>::iterator it = children.begin(); it != children.end(); ++it)
       { 
         id = mb->id_from_handle(*it);
         std::cout << "   Child Id: " << id << "\n";
       }

    }
  return rval;
}

//------------------------------------------------------------------------------
ErrorCode MOABSelectionAlgorithm::CreateSelectionTree(Interface *mb, Range ents,Tag stag, std::vector<std::string> vstr)
{
  ErrorCode rval;
  char tag_data[CATEGORY_TAG_SIZE];
  for(Range::iterator i = ents.begin(); i != ents.end(); ++i)
    {
      rval = mb->tag_get_data(stag,&*i,1,&tag_data);
      std::string str = std::string(tag_data);
      if (std::find(vstr.begin(), vstr.end(),str) != vstr.end())
        {
          vstr.push_back(str);
        }
      return rval;
    }
}

//------------------------------------------------------------------------------
ErrorCode MOABSelectionAlgorithm::TagLowLevelInfo(Interface *mb, Range tree, std::vector<std::string> allNames)
{
  ErrorCode rval;

  // Grab the vertices and tag them with a tag corresponding to what categories
  // and NAME_TAGS they are a part of.
  Range verts;
  rval = mb->get_entities_by_dimension(0, 0, verts);
  Range parents;
  Tag catTag, nameTag;
  unsigned long int bitTags;
  Tag selTag;
  for ( Range::iterator it = verts.begin(); it != verts.end(); ++it )
    {
      Range test;
      rval = mb->get_parent_meshsets(*it, parents, 0);
      rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, catTag);
      rval = mb->tag_get_handle(NAME_TAG_NAME, NAME_TAG_SIZE, MB_TYPE_OPAQUE, nameTag);
      std::vector<Tag> tag_handles;
      for ( Range::iterator i = parents.begin(); i != parents.end(); ++i)
        {
          rval = mb->tag_get_tags_on_entity(*i, tag_handles);
          for (std::vector<Tag>::iterator j = tag_handles.begin(); j != tag_handles.end(); ++j)
            {
              unsigned long int bitTag;
              if( *j == catTag )
                {
                  std::vector<std::string>::iterator iter;
                  char tag_data[CATEGORY_TAG_SIZE];
                  rval = mb->tag_get_data(*j, &*i, 1, tag_data);
                  iter = std::find(allNames.begin(),allNames.end(),std::string(tag_data));
                  bitTag = std::pow(2,std::distance(allNames.begin(),iter));
                }
              if( *j == nameTag )
                {
                  std::vector<std::string>::iterator iter;
                  char tag_data[NAME_TAG_SIZE];
                  std::string sTagData = std::string(tag_data);
                  rval = mb->tag_get_data(*j, &*i, 1, tag_data);
                  iter = std::find(allNames.begin(),allNames.end(),sTagData);
                  std::string iter2 = std::string(*iter);
                  if(iter2.compare(sTagData) && !(sTagData.compare(*(allNames.end()))))
                    {
                      break;
                    }
                  bitTag = std::pow(2,std::distance(allNames.begin(),iter));
                }
              bitTags = bitTags | bitTag;
              rval = mb->tag_get_handle(HIERARCHY_TAG_NAME,MB_TAG_VARLEN, MB_TYPE_OPAQUE, selTag, MB_TAG_CREAT|MB_TAG_EXCL);
              rval = mb->tag_set_data(selTag,&*it,1,&bitTags);
            }
        }
    }
  return rval;
}

//------------------------------------------------------------------------------
ErrorCode MOABSelectionAlgorithm::GetDesiredEntity(Interface *mb, unsigned long int selMode, Range eh)
{
  ErrorCode rval;
  Tag sTag;
  unsigned long int selTag;
  int cat_match = 0;
  rval = mb->tag_get_handle(HIERARCHY_TAG_NAME, MB_TAG_VARLEN, MB_TYPE_OPAQUE, sTag);
  for ( Range::iterator i = eh.begin(); i != eh.end(); ++i)
    {
      rval = mb->tag_get_data(sTag,&*i,1,&selTag);
      if ( selTag & selMode )
        {
          cat_match += 1; 
        }
   }
  return rval;
}
