char group_category[CATEGORY_TAG_SIZE];
for(std::vector<std::string>::iterator i = vStr.begin(); i != vStr.end(); ++i )
  {
    std::fill(group_category, group_category+CATEGORY_TAG_SIZE, '\0');
    sprintf(group_category, "%s", *i);
    const void* const group_val[] = {&group_category};
    Range groups;
    rval = mb->get_entities_by_type_and_tag(0,MBENTITYSET, &catNameTag, group_val, 1, groups);
    Range tagVerts;
    for(Range::iterator j = groups.begin(); j != groups.end(); ++j)
      {
        Range verts;
        rval = mb->get_entities_by_type(*j,MBVERTEX,verts);
