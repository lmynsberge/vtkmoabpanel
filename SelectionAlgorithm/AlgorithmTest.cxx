#include <iostream>  
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include "./MOABSelectionAlgorithm.h"

using namespace moab;
using namespace std;

int main(int argc, char** argv)
{
  // Check to make sure there's a geometry file.
  if (argc < 2)
    {
      std::cout << "Test requires a .h5m file\n";
      exit(0);
    }
  // Begin the checks of the algorithm.
  else
    {
      // Instantiate a new interface.
      moab::Interface *mb = new moab::Core;
      const char* moabFile;
      // Get the file name and load it.
      moabFile = argv[1];
      cout << "Loading File Name: " << moabFile << "\n";
      ErrorCode rval = mb->load_file(moabFile);
      assert(rval == MB_SUCCESS);

      std::cout << "Loading Information Tree...\n";
      MOABSelectionAlgorithm *a = new MOABSelectionAlgorithm();
      std::map<std::string, std::vector<std::string> > ehMap;
      std::vector<std::string> vecStr;
      a->BitTagVertex = true;

      /* Start time */
      double t = 0.0;
      clock_t start = clock();

      std::cout << "Got Here." << "\n"; // Worked thus far
      rval = a->MOABSelectionAlgorithm::LoadCatNameTags(mb,ehMap);
      t = (double) (clock()-start)/CLOCKS_PER_SEC;
      std::cout << "Time (Loading Cat Tags): " << t << "\n";
 
      a->MOABSelectionAlgorithm::get_category_tags(ehMap, vecStr);

      std::cout << "Categories: \n";
      for(std::vector<std::string>::iterator it = vecStr.begin(); it != vecStr.end(); ++it)
        {
          std::cout << *it << "\n";
        }

      start = clock();
      rval = a->MOABSelectionAlgorithm::BitTagVertices(mb,vecStr);
      t = (double) (clock()-start)/CLOCKS_PER_SEC;
      std::cout << "Time (Bit Tagging Entities): " << t << "\n";

      std::vector<Tag> allTags;
      rval = mb->tag_get_tags(allTags);
      for(std::vector<Tag>::iterator i = allTags.begin(); i != allTags.end(); ++i)
        {
          std::string tagName;
          moab::Tag atag = *i;
          moab::Range ents;
          rval = mb->tag_get_name(atag,tagName);
          std::cout << "Tag Name: " << tagName << "\n";
          if(tagName == "MATERIAL_SET")
            {
              mb->get_entities_by_type_and_tag(0, MBENTITYSET, atag, NULL, 1, ents, Interface::UNION);
              char * data = new char;
              for(moab::Range::iterator j = ents.begin(); j != ents.end(); ++j)
                {
                  mb->tag_get_data(atag,*j,data);
                  std::cout << "Tag Data: " << *data << "\n";
                }
            }
        }



      
      // Test Selection Speed
      Range bestAlgorithmGuess;
      long unsigned int mode = 36;//  4608; // This will select only surfaces: 1001000000000
      Range selection; // We'll create this below, by first selecting one surface we know is in there.

      // Here we get the selection
      Tag tag;
      Range ents;
      rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, tag);

      // Query all entity sets for category tag value of surface and just grab the first surface to be used in the algorithm.
      /*
      char charIndex[CATEGORY_TAG_SIZE];
      std::fill(charIndex, charIndex+CATEGORY_TAG_SIZE, '\0');
      sprintf(charIndex, "%s", "Surface");
      const void* const group_val[] = {&charIndex};
      rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &tag, group_val, 1, ents, 
          Interface::UNION);
      rval = mb->get_entities_by_handle(ents.front(),selection);
      std::cout << "Grabbed Surface #" << mb->id_from_handle(ents.front()) << "\n";
      //*/
      Range smallSel;

      // Query all the entity sets and get all the entities involved.
      rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &tag, NULL, 1, ents, Interface::UNION);
      std::cout << "Number of Entity Sets: " << ents.size() << "\n";
      for(Range::iterator i = ents.begin(); i != ents.end(); ++i)
        {
          rval = mb->get_entities_by_handle(*i,selection);
          smallSel.merge(selection);
        }

      /*
      smallSel.insert(selection.pop_front());
      smallSel.insert(selection.pop_front());
      smallSel.insert(selection.pop_front());
      */

      std::cout << "Selection has: " << smallSel.size() << " entities in it." << "\n";
      start = clock();     
      // Alright give this information
      rval = a->MOABSelectionAlgorithm::GetSelection(mb, mode, smallSel, bestAlgorithmGuess, vecStr);
      t = (double) (clock()-start)/CLOCKS_PER_SEC;
      std::cout << "Time (GetSelection): " << t << "\n";

      // This should definitely give back the proper surface since it has all the vertices composing that surface. We'll check.
      int selSurf = mb->id_from_handle(ents.front());
      std::cout << "Size of Guess: " << bestAlgorithmGuess.size() << "\n";
      for(Range::iterator jj = bestAlgorithmGuess.begin(); jj != bestAlgorithmGuess.end(); ++jj)
        {
          int givenSurf = mb->id_from_handle(*jj);
          std::cout << "Selected Entity: " << selSurf << " Algorithm says: " << givenSurf << "\n";
        }

      return 0;
    }
}
