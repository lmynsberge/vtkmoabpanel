# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/lucas/Moab/tools/vtkMoabPanel/SelectionAlgorithm/MOABSelectionAlgorithm.cxx" "/home/lucas/BitBucket/moab/tools/vtkMoabPanel/SelectionAlgorithm/CMakeFiles/MOABSelectionAlgorithm.dir/MOABSelectionAlgorithm.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/lucas/Moab/tools/vtkMoabPanel/SelectionAlgorithm/SelectionAlgorithm"
  "/home/lucas/Downloads/MOAB"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
