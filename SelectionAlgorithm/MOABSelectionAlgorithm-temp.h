#include "MBTagConventions.hpp"
#include "moab/CartVect.hpp"
#include "moab/Range.hpp"
#include "moab/Core.hpp"
#include "moab/GeomUtil.hpp"
#include "moab/Interface.hpp"

using namespace moab;
//------------------------------------------------------------------------------
class MOABSelectionAlgorithm
{
 public:
  moab::Interface *Moab;
  ErrorCode LoadInformation(moab::Interface *mb);
  ErrorCode CreateSelectionTree(Interface *mb, Range ents,Tag stag, std::vector<std::string> vstr);
  ErrorCode TagLowLevelInfo(Interface *mb, Range tree, std::vector<std::string> allNames);
    ErrorCode GetDesiredEntity(Interface *mb, unsigned long int selMode, Range eh);

  MOABSelectionAlgorithm();
  ~MOABSelectionAlgorithm();

};

