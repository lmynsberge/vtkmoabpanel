# Welcome to the vtkMOABPanel Repository #

## This Repo Contains ParaView Plugins to Increase MOAB Visualization ##

### Contents and Summary: ###
1. vtkMOABReaderNew - This directory contains the plugin necessary to read the MOAB files    and processes the input into ParaView format.

2. vtkMOABWidget - This directory is the viewer, which allows the viewing of the reader's    imported data from the MOAB model.

3. vtkMOABFilter - This directory contains the plugin that writes the tags that will be ad   ded onto MOAB entities or entity sets.

