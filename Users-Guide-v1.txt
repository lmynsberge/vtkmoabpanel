This section describes the installation process for the ParaView plugins 
designed to facilitate MOAB manipulation and labeling of 3D geometries. 
It starts by preparing the computer by installing the necessary 
prerequisites, then installing ParaView, and finally adding the plugins. 
For this install, I will assume that you have a Unix-like operating 
system. For the "Prerequisites" section, there are three main options. 
1) Download the newest source code or tarball and compile and install 
it, 2) Download the most applicable binary and install it, 3) Use 
"aptitude" or "yum" to quickly install the program from the terminal. 


I.	Prerequisites
	a.	Install CMake
		i.	Go to http://www.cmake.org/ and download the most recent version of the CMake.
	b.	Install Qt
		i.	Go to http://qt-project.org/ and download the most recent version of Qt. Use the open-source version and preferably the development version in order to get all of the libraries.
II.	Download and Install ParaView
	a.	Install ParaView
		i.	Go to http://paraview.org/paraview/resources/software.php and download version 3.98 of ParaView (although the newest version of 4.0 should still allow it to run normally). Although there is supposed to be a binary that can support plugins, I suggest that you download the source code and compile/install on your own.
		ii.	Un-tar your source code. 
		iii.	Make a build directory somewhere separate from your source directory.
		iv.	Type "ccmake $PARAVIEW_SRC" where $PARAVIEW_SRC is your source directory.
		v.	Turn "ON" "BUILD_SHARED_LIBS" as an option in the ccmake GUI. You may also set any more advanced options as desired. For some more details, see http://paraview.org/Wiki/ParaView:Build_And_Install.
		vi.	Iteratively press "c" until you have configured with the exact options and proper directories for all necessary dependencies.
		vii.	Type "g" to generate the makefile.
		viii.	Now type "make" and "make install" (might need to be root). The process usually takes sometime.
	b.	Install MOAB
		i.	Go to https://trac.mcs.anl.gov/projects/ITAPS/wiki/MOAB and download and install the most recent MOAB.
	c.	Build the ParaView Plugins 
		i.	Clone or fork the plugins from https://bitbucket.org/lmynsberge/vtkmoabpanel/. Or use git@bitbucket.org:lmynsberge/vtkmoabpanel.git.
		ii.	This contains the three folders and three components of the project.
		iii.	Create a new build directory for these.
		iv.	Type "ccmake $PLUGIN_SRC," where $PLUGIN_SRC is the source directory of the repository source code for one of the plugins.
		v.	Set the options and locations of necessary dependencies through the iterative "c" option.
		vi.	Use "g" to generate the Makefile.
		vii.	Then use "make" to build the library.
		viii.	Repeat for the other two plugins.
	d.	Add the Plugins to ParaView
		i.	Open ParaView. Under the "Tools" header go to "Manage Plugins.."
		ii.	Load the plugins by navigating to the libraries in the build directory after clicking "Load New."
	e.	Utilize Plugins
		i.	Open your favorite ".h5m" file.
		ii.	Make a selection.
		iii.	Extract and label that selection by using the "Tag Block Filter" that is now in the filters menu.
